FROM registry.cern.ch/docker.io/library/alpine

RUN apk add logrotate bash findutils \
	&& rm -rf /var/cache/apk/* && rm -rf /tmp/*

COPY podmanlog /etc/logrotate.d/podmanlog
COPY podmanlog.sh /usr/local/bin/podmanlog.sh

ENTRYPOINT ["/usr/local/bin/podmanlog.sh"]
