#!/bin/bash

sleep_time="300s"

cat > /etc/containers/containers.conf <<EOF
[containers]
log_size_max = 50000000
EOF

set -x
while true
do
	logrotate -fv /etc/logrotate.d/podmanlog
	find /hostfs/var/lib/containers/ -type f -name ctr.log* | xargs ls -lhS
	sleep ${sleep_time}
done

